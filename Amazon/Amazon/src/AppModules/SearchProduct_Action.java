package AppModules;
 
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import Utility.ExcelUtils;
 
// Now this method does not need any arguments
public class SearchProduct_Action {
 
	public static void Execute(WebDriver driver) throws Exception{
 
		//This is to get the values from Excel sheet, passing parameters (Row num &amp; Col num)to getCellData method
		String sProduct = ExcelUtils.getCellData(1, 1);
		
		driver.findElement(By.id("twotabsearchtextbox")).sendKeys(sProduct);
		driver.findElement(By.cssSelector("input.nav-input")).click();
		
		// Select item
		driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div/div[1]/div/span[3]/div[2]/div[3]/div/span/div/div/div[2]/div[2]/div/div[1]/div/div/div[1]/h2/a/span")).click();
		
		// Add to Cart
		driver.findElement(By.id("add-to-cart-button")).click();
    }
 
}